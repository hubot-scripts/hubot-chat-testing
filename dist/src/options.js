"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HubotChatOptions {
    constructor() {
        this.answerDelay = 50; // How long the we should wait for the answer (i.e. when the bot uses some HTTP retrieving data)
    }
}
exports.HubotChatOptions = HubotChatOptions;
